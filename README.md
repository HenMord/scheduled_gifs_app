To run this project please do the following:
1. Checkout the repository
2. Browse to projects root folder
3. Run npm install
4. Run npm run start (local host on port 3000 will be opened)

Main Sources:
- App.js - main source, contains most of the logic
- InputForm.js - component for input form
- JobsGallery.js - component for gallery

*Note: known limitation for giphy-api - limit of 1000 queries per day