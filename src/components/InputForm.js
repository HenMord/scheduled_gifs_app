import React, { Component } from "react";
import Form from "react-bootstrap/Form";
import Button from "react-bootstrap/Button";
import Container from "react-bootstrap/Container";

class InputForm extends Component {
  constructor() {
    super();
    this.state = {
      subject: "",
      frequency: "",
    };
  }

  render() {
    return (
      <div className="center_div">
        <Container>
          <Form>
            <Form.Group controlId="formBasicEmail">
              <Form.Label>Subject</Form.Label>
              <Form.Control
                type="text"
                placeholder="Enter subject"
                onChange={(e) => this.setState({ subject: e.target.value })}
                value={this.state.subject}
              />
            </Form.Group>
            <Form.Group controlId="formBasicEmail">
              <Form.Label>Frequency</Form.Label>
              <Form.Control
                type="text"
                placeholder="Enter frequency (seconds)"
                onChange={(e) => this.setState({ frequency: e.target.value })}
                value={this.state.frequency}
              />
            </Form.Group>
            <Button
              variant="primary"
              type="submit"
              onClick={this.afterSubmission}
            >
              Submit
            </Button>
          </Form>
        </Container>
      </div>
    );
  }

  afterSubmission = (event) => {
    event.preventDefault();
    if (!this.isFormValid()) {
      alert("Form not valid! all fields should be filled");
    } else {
      this.props.submitFunc(
        this.state.subject,
        parseInt(this.state.frequency) * 1000
      );
    }
    this.setState({ subject: "", frequency: "" });
  };

  isFormValid = () => {
    return this.state.subject !== "" &&
      this.state.frequency !== "" &&
      !isNaN(this.state.frequency);
  };
}

export default InputForm;
