import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import GridList from "@material-ui/core/GridList";
import GridListTile from "@material-ui/core/GridListTile";
import GridListTileBar from "@material-ui/core/GridListTileBar";
import Button from "react-bootstrap/Button";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";

function JobsGallery(props) {
  let classes = styles();
  return (
    <div className={classes.root}>
      <Container fluid>
        <Row>
          <Col>
            <h2 text-align="center" padding="10px">
              Gallery
            </h2>
          </Col>
        </Row>
        {props.jobs.map((job) => (
          <Row key={job.jobId}>
            <Col xs={1} className="align-self-center">
              Job's subject: {job.subject}
              <Button
                variant="primary"
                onClick={() => {
                  props.cancelJob(job);
                }}
              >
                Cancel Job
              </Button>
            </Col>
            <Col xs={10}>
              <GridList className={classes.gridList} cols={5.5}>
                {props.allSubjectsGifs[job.jobId]
                  .slice(0) //makes a copy
                  .reverse()
                  .map((tile) => (
                    <GridListTile key={tile.src}>
                      <img src={tile.src} alt={tile.title} />
                      <GridListTileBar
                        title={tile.title}
                        classes={{
                          root: classes.titleBar,
                          title: classes.title,
                        }}
                      />
                    </GridListTile>
                  ))}
              </GridList>
            </Col>
          </Row>
        ))}
      </Container>
    </div>
  );
}

const styles = makeStyles((theme) => ({
    root: {
      display: "flex",
      flexWrap: "wrap",
      justifyContent: "space-around",
      overflow: "hidden",
      backgroundColor: theme.palette.background.paper,
    },
    gridList: {
      flexWrap: "nowrap",
      // Promote the list into his own layer on Chrome. This cost memory but helps keeping high FPS.
      transform: "translateZ(0)",
    },
    title: {
      color: theme.palette.primary.light,
    },
    titleBar: {
      background:
        "linear-gradient(to top, rgba(0,0,0,0.7) 0%, rgba(0,0,0,0.3) 70%, rgba(0,0,0,0) 100%)",
    },
  }));
  

export default JobsGallery;
