import React, { Component } from "react";
import "./App.css";
import InputForm from "./components/InputForm";
import JobsGallery from "./components/JobsGallery";
import uuidv4 from "uuid";

import "bootstrap/dist/css/bootstrap.min.css";

const giphy = require("giphy-api")("flzsGxrDmp88BdW46arN7XPQ3JJlGV3L");
// max number of gifs to fetch in a request (defined by Giphy)
const batch_size = 100;
const refill_size = 2;
const success_status = 200;

class App extends Component {
  constructor() {
    super();
    // takes state from local storage if exists
    if (localStorage.getItem("jobsData") !== null) {
      this.state = JSON.parse(localStorage.getItem("jobsData"));
      this.restoreIntervals();
    } else {
      this.state = {
        jobs: [],
        presentedGifs: {},
        queuedGifs: {},
      };
    }
  }

  // called only from contructor, hence not using setState
  restoreIntervals = () => {
    for (let i = 0; i < this.state.jobs.length; i++) {
      // eslint-disable-next-line
      this.state.jobs[i].timerId = setInterval(
        this.fetchGIFs,
        this.state.jobs[i].frequency,
        this.state.jobs[i].jobId,
        this.state.jobs[i].subject
      );
    }
  };

  render() {
    return (
      <div className="App">
        <h1 className="Main-title">Gifs App</h1>
        <InputForm submitFunc={this.submitFunc} />
        <JobsGallery
          jobs={this.state.jobs}
          allSubjectsGifs={this.state.presentedGifs}
          cancelJob={this.cancelJob}
        />
      </div>
    );
  }

  componentDidUpdate = () => {
    // save in local storage after render
    localStorage.setItem("jobsData", JSON.stringify(this.state));
  };

  submitFunc = (subject, freq) => {
    const uuid = uuidv4.v4();
    this.fillQueue(uuid, subject, true);
    let timerId = setInterval(this.fetchGIFs, freq, uuid, subject);
    this.registerJob(uuid, timerId, subject, freq);
  };

  /// Draws gifs from Giphy into local queue
  fillQueue = (uuid, subject, firstCallPerJob) => {
    let offset;
    if (firstCallPerJob) {
      offset = 0;
    } else {
      offset =
        this.state.queuedGifs[uuid].length +
        this.state.presentedGifs[uuid].length;
    }
    giphy.search(
      {
        q: subject,
        limit: batch_size,
        fmt: "json",
        offset: offset,
      },
      (err, res) => {
        if (res !== null && res.data.length !== 0 && res.meta.status === success_status) {
          this.addGifsToQueue(uuid, res);
        }
      }
    );
  };

  registerJob = (uuid, timerId, sub, freq) => {
    let job = {
      jobId: uuid,
      timerId: timerId,
      subject: sub,
      frequency: freq,
    };
    this.setState({
      jobs: [...this.state.jobs, job],
      presentedGifs: { ...this.state.presentedGifs, [uuid]: [] },
    });
  };

  addGifsToQueue = (uuid, fetchedData) => {
    let gifs_data = fetchedData.data.map((gif) => {
      return {
        src: gif.images.downsized.url,
        title: gif.title,
      };
    });
    // check if uuid was alreay added
    if (uuid in this.state.queuedGifs) {
      this.setState({
        queuedGifs: {
          ...this.state.queuedGifs,
          [uuid]: this.state.queuedGifs[uuid].concat(gifs_data),
        },
      });
    } else {
      this.setState({
        queuedGifs: { ...this.state.queuedGifs, [uuid]: gifs_data },
      });
    }
  };

  fetchGIFs = (uuid, subject) => {
    // copy to avoid mutating state directly
    let copyOfGifsArray = [...this.state.presentedGifs[uuid]];
    let copyOfQueuedGifs = { ...this.state.queuedGifs };

    // check if fetching from giphy succeeded
    if (uuid in copyOfQueuedGifs && copyOfQueuedGifs[uuid].length > 0) {
      this.moveGifsToPresented(copyOfGifsArray, copyOfQueuedGifs, uuid);

      // fill queue if about to empty
      if (this.state.queuedGifs[uuid].length < refill_size) {
        this.fillQueue(uuid, subject, false);
      }
    } else {
      this.handleNoAvailGifs(subject, uuid);
    }
  };

  moveGifsToPresented = (presentedGifsOfJob, queuedGifs, uuid) => {
    presentedGifsOfJob.push(queuedGifs[uuid][0]);
    queuedGifs[uuid].splice(0, 1);
    this.setState({
      presentedGifs: {
        ...this.state.presentedGifs,
        [uuid]: [...this.state.presentedGifs[uuid], queuedGifs[uuid][0]],
      },
      queuedGifs: queuedGifs,
    });
  };

  handleNoAvailGifs = (subject, uuid) => {
    alert(`No available GIFS, job of subject ${subject} is canceled!`);
    let job = this.state.jobs.find((job) => uuid === job.jobId);
    if (Object.keys(job).length !== 0) {
      this.cancelJob(job);
    }
  };

  cancelJob = (job) => {
    // copy to avoid mutating state directly
    let copyOfPresentedGifs = { ...this.state.presentedGifs };
    delete copyOfPresentedGifs[job.jobId];

    let copyOfQueuedGifs = { ...this.state.queuedGifs };
    delete copyOfQueuedGifs[job.jobId];

    let copyOfJobs = [...this.state.jobs];
    let index = copyOfJobs.indexOf(job);
    if (index !== -1) {
      clearInterval(job.timerId);
      copyOfJobs.splice(index, 1);
      this.setState({
        jobs: copyOfJobs,
        presentedGifs: copyOfPresentedGifs,
        queuedGifs: copyOfQueuedGifs,
      });
    }
  };
}

export default App;
